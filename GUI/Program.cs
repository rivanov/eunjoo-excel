﻿using ClosedXML.Excel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GUI
{
    class HashMapResult
    {
        public HashMapResult ()
        {
            Result = new Dictionary<string, List<double>>();
            Duplicates = new List<Tuple<string, List<double>>>();
        }
        public Dictionary<string, List<double>> Result { get; set; }
        public List<Tuple<string, List<double>>> Duplicates { get; set; }
        public IXLRow headerRow { get; set; }
    }

    class Program
    {
        public delegate void DuplicateEventHandler(object sender, DuplicateEventArgs e);
        public event DuplicateEventHandler DuplicateAdded;
        public class DuplicateEventArgs : EventArgs
        {
            public FileInfo file;
            public IXLRow row;
            public string geneName;

            public DuplicateEventArgs(FileInfo file, IXLRow row, string geneName)
            {
                this.file = file;
                this.row = row;
                this.geneName = geneName;
            }
        }

        public const string inputDir = @"..\..\..\input2\";
        public const string outputDir = @"..\..\..\output\";
        public string csvInputFileName = "munged_CCA_CCS_FPA_FPS_high-combined-FPKM.csv";
        public string xmlInputFileName = "munged_CCA_CCS_FPA_FPS_high-combined-FPKM.xlsx";
        public string inputPath, convertedPath;
        private FileInfo inputFile;
        public Form1 GUI = null;

        /// <summary>
        /// Initialized and owned by the GUI
        /// </summary>
        public Program()
        {
            inputPath = inputDir + csvInputFileName;
            convertedPath = inputDir + xmlInputFileName;
        }

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            var form = new Form1();
            Application.Run(form);
        }

        public HashMapResult GenerateHashMap(XLWorkbook workbook)
        {
            var result = new HashMapResult();
            var workSheet = workbook.Worksheet(1);                                  // We work with the first worksheet in the XLSX file                                                     
            var headerRow = workSheet.Row(1);                                           // Get the header row

            var columnOne = workSheet.Column(1);                                    // Select the 1st column, from the 1st worksheet in the workbook
            List<string> geneNames = new List<string>();                            // List to hold the gene names generate from columnOne
            Dictionary<string, List<double>> hashTable =                            // Hash table of gene names --> Lists of their values
                new Dictionary<string, List<double>>();
            var colOneCells =                                                       // Get all the "rows", in columnOne
                columnOne.Cells(                                                    // By extracting all of that column's cells
                    2,                                                              // Exclude the 1st row
                    columnOne.Cells().Count());                                     // Get the last row
            foreach (var name in colOneCells)                                       // Loop through the 2nd to the last row in the 1st column
                geneNames.Add(name.RichText.Text);                                  // Add the gene name as a string to the geneList

            foreach (var row in colOneCells)                                   // Loop through the 2nd to the last row in the 1st column
            {
                try
                {
                    hashTable.Add(row.RichText.Text,                               // Create an entry for this name in the hashtable
                        new List<double>());                                        // Initialize that entry's list to a blank, new list
                }
                catch (ArgumentException e)
                {
                    // Show this message in the status bar, and have a list of duplicates that can be viewed

                    var duplicate = new Tuple<string, List<double>>
                        (row.RichText.Text, new List<double>());
                    result.Duplicates.Add(duplicate);
                    var eventArgs = new DuplicateEventArgs(this.inputFile, row.WorksheetRow(), row.RichText.Text);
                    DuplicateAdded(this, eventArgs);                     // Fire off the duplicate added event to update the UI's status bar
                    continue;                                                   // Skip this gene and try the next
                    throw e;
                }
            }


            foreach (var row in                                                 // Get the row for each gene
                                workSheet.Rows(2, workSheet.RowsUsed().Count()))        // By selecting all the rows but the 1st one
            {

                var geneName = row.Cell(1).RichText.Text;                       // Find the current gene name from the 1st column
                List<double> list = new List<double>();
                // Pick out the rows for that gene
                var rowCells = row.Cells(2,                                     // Consisting of the 2nd column 
                                    row.CellsUsed().Count());                       // To the end column
                foreach (var cell in rowCells)

                {
                    double cellValue = double.NaN;                              // Store each cell's value as a double

                    if (double.TryParse(cell.RichText.Text, out cellValue))     // Try to parse that double into our value
                        list.Add(cellValue);                                    // If successful, add to our list
                                                                                //else list.Add(double.NaN);                                  // Otherwise, signify failure with NaN

                }                                                               // End of loop which traverses each row's cells

                hashTable[geneName] = list;                                     // Set the gene's list
            }                                                                   // End of loop which traverses each workbooks rows

            result.headerRow = headerRow;
            result.Result = hashTable;                                                   // Return our completed work
            return result;
        }

        public async Task<HashMapResult> GenerateHashMap(string inputFile)
        {
            this.inputFile = new FileInfo(inputFile);
            XLWorkbook workbook = null;                                             // Initialize a new workbook
            if (this.GUI.CSV)                                                       // If we are given CSV input
                workbook = CovertToWorkbook(inputFile, "1",                         // Covert the CSV file by looking at it's 1st worksheet
                    ReadCsv(inputFile, ','));                                       // Split the CSV file into a string array
            else if (this.GUI.XLSX)                                                 // If we're given an XLSX file
                workbook = new XLWorkbook(inputFile);                               // Straight up, create the workbook from that filename

            return GenerateHashMap(workbook);
        }

        /// <summary>
        /// 
        /// TODO: Make generic so that it can covert all files from a given directory
        /// </summary>
        /// <param name="excelFileName"></param>
        /// <param name="worksheetName"></param>
        /// <param name="csvLines"></param>
        /// <returns></returns>
        public XLWorkbook CovertToWorkbook(string excelFileName, string worksheetName, IEnumerable<string[]> csvLines)
        {
            if (csvLines == null || csvLines.Count() == 0)
            {
                return null;
            }

            int rowCount = 0;
            int colCount = 0;

            using (var workbook = new XLWorkbook())
            {
                using (var worksheet = workbook.Worksheets.Add(worksheetName))
                {
                    rowCount = 1;
                    foreach (var line in csvLines)
                    {
                        colCount = 1;
                        foreach (var col in line)
                        {
                            worksheet.Cell(rowCount, colCount).Value = TypeConverter.TryConvert(col);
                            colCount++;
                        }
                        rowCount++;
                    }

                }
                //workbook.SaveAs(excelFileName);
                return workbook;
            }
        }

        public static IEnumerable<string[]> ReadCsv(string fileName, char delimiter = ';')
        {
            var lines = System.IO.File.ReadAllLines(fileName, Encoding.UTF8).Select(a => a.Split(delimiter));
            return (lines);
        }
    }
}
