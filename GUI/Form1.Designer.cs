﻿namespace GUI
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ArrowAnnotation arrowAnnotation2 = new System.Windows.Forms.DataVisualization.Charting.ArrowAnnotation();
            System.Windows.Forms.DataVisualization.Charting.TextAnnotation textAnnotation2 = new System.Windows.Forms.DataVisualization.Charting.TextAnnotation();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea3 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea4 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend3 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Legend legend4 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint11 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(0D, 1D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint12 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(0D, 2D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint13 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(0D, 3D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint14 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(0D, 4D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint15 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(0D, 5D);
            System.Windows.Forms.DataVisualization.Charting.Series series4 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint16 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(0D, 6D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint17 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(0D, 7D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint18 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(0D, 8D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint19 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(0D, 9D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint20 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(0D, 10D);
            System.Windows.Forms.DataVisualization.Charting.Title title2 = new System.Windows.Forms.DataVisualization.Charting.Title();
            this.m_cmbBox = new System.Windows.Forms.ComboBox();
            this.chartMain = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.mnuMain = new System.Windows.Forms.MenuStrip();
            this.tsmiInputFileType = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiCSV = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiXLSX = new System.Windows.Forms.ToolStripMenuItem();
            this.m_btnGenerateGeneList = new System.Windows.Forms.Button();
            this.m_btnGenerateColumnList = new System.Windows.Forms.Button();
            this.m_statusStrip = new System.Windows.Forms.StatusStrip();
            this.m_tsslError = new System.Windows.Forms.ToolStripStatusLabel();
            this.m_tsProgressBar = new System.Windows.Forms.ToolStripProgressBar();
            this.m_lvDuplicates = new System.Windows.Forms.ListView();
            this.chFileName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.m_chRowNumber = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.m_chGeneName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            ((System.ComponentModel.ISupportInitialize)(this.chartMain)).BeginInit();
            this.mnuMain.SuspendLayout();
            this.m_statusStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // m_cmbBox
            // 
            this.m_cmbBox.FormattingEnabled = true;
            this.m_cmbBox.Location = new System.Drawing.Point(47, 85);
            this.m_cmbBox.Name = "m_cmbBox";
            this.m_cmbBox.Size = new System.Drawing.Size(180, 21);
            this.m_cmbBox.TabIndex = 0;
            this.m_cmbBox.SelectionChangeCommitted += new System.EventHandler(this.m_cmbBox_SelectionChangeCommitted);
            this.m_cmbBox.TextChanged += new System.EventHandler(this.m_cmbBox_TextChanged);
            // 
            // chartMain
            // 
            arrowAnnotation2.AllowTextEditing = true;
            arrowAnnotation2.AnchorDataPointName = "Series1\\r0";
            arrowAnnotation2.AnchorOffsetY = -2.5D;
            arrowAnnotation2.Height = -5D;
            arrowAnnotation2.Name = "ArrowAnnotation1";
            arrowAnnotation2.SmartLabelStyle.IsOverlappedHidden = false;
            arrowAnnotation2.Width = 0D;
            textAnnotation2.AnchorDataPointName = "Series1\\r1";
            textAnnotation2.Name = "TextAnnotation1";
            textAnnotation2.Text = "Cool Data";
            this.chartMain.Annotations.Add(arrowAnnotation2);
            this.chartMain.Annotations.Add(textAnnotation2);
            chartArea3.Name = "ChartArea1";
            chartArea4.Name = "ChartArea2";
            this.chartMain.ChartAreas.Add(chartArea3);
            this.chartMain.ChartAreas.Add(chartArea4);
            legend3.DockedToChartArea = "ChartArea1";
            legend3.IsDockedInsideChartArea = false;
            legend3.Name = "Legend1";
            legend4.DockedToChartArea = "ChartArea2";
            legend4.IsDockedInsideChartArea = false;
            legend4.Name = "Legend2";
            this.chartMain.Legends.Add(legend3);
            this.chartMain.Legends.Add(legend4);
            this.chartMain.Location = new System.Drawing.Point(46, 119);
            this.chartMain.Name = "chartMain";
            series3.ChartArea = "ChartArea1";
            series3.Legend = "Legend1";
            series3.Name = "Series1";
            series3.Points.Add(dataPoint11);
            series3.Points.Add(dataPoint12);
            series3.Points.Add(dataPoint13);
            series3.Points.Add(dataPoint14);
            series3.Points.Add(dataPoint15);
            series4.ChartArea = "ChartArea2";
            series4.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Bar;
            series4.Legend = "Legend2";
            series4.Name = "BarChart";
            series4.Points.Add(dataPoint16);
            series4.Points.Add(dataPoint17);
            series4.Points.Add(dataPoint18);
            series4.Points.Add(dataPoint19);
            series4.Points.Add(dataPoint20);
            this.chartMain.Series.Add(series3);
            this.chartMain.Series.Add(series4);
            this.chartMain.Size = new System.Drawing.Size(453, 248);
            this.chartMain.TabIndex = 1;
            this.chartMain.Text = "chart1";
            title2.Name = "Title1";
            title2.Text = "Chart 1";
            this.chartMain.Titles.Add(title2);
            // 
            // mnuMain
            // 
            this.mnuMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiInputFileType});
            this.mnuMain.Location = new System.Drawing.Point(0, 0);
            this.mnuMain.Name = "mnuMain";
            this.mnuMain.Size = new System.Drawing.Size(582, 24);
            this.mnuMain.TabIndex = 2;
            this.mnuMain.Text = "menuStrip1";
            // 
            // tsmiInputFileType
            // 
            this.tsmiInputFileType.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiCSV,
            this.tsmiXLSX});
            this.tsmiInputFileType.Name = "tsmiInputFileType";
            this.tsmiInputFileType.Size = new System.Drawing.Size(91, 20);
            this.tsmiInputFileType.Text = "InputFileType";
            // 
            // tsmiCSV
            // 
            this.tsmiCSV.Checked = true;
            this.tsmiCSV.CheckState = System.Windows.Forms.CheckState.Checked;
            this.tsmiCSV.Name = "tsmiCSV";
            this.tsmiCSV.Size = new System.Drawing.Size(100, 22);
            this.tsmiCSV.Text = "CSV";
            // 
            // tsmiXLSX
            // 
            this.tsmiXLSX.Name = "tsmiXLSX";
            this.tsmiXLSX.Size = new System.Drawing.Size(100, 22);
            this.tsmiXLSX.Text = "XLSX";
            // 
            // m_btnGenerateGeneList
            // 
            this.m_btnGenerateGeneList.Location = new System.Drawing.Point(46, 56);
            this.m_btnGenerateGeneList.Name = "m_btnGenerateGeneList";
            this.m_btnGenerateGeneList.Size = new System.Drawing.Size(181, 23);
            this.m_btnGenerateGeneList.TabIndex = 3;
            this.m_btnGenerateGeneList.Text = "Generate Gene List";
            this.m_btnGenerateGeneList.UseVisualStyleBackColor = true;
            this.m_btnGenerateGeneList.Click += new System.EventHandler(this.m_btnGenerateGeneList_Click);
            // 
            // m_btnGenerateColumnList
            // 
            this.m_btnGenerateColumnList.Location = new System.Drawing.Point(233, 83);
            this.m_btnGenerateColumnList.Name = "m_btnGenerateColumnList";
            this.m_btnGenerateColumnList.Size = new System.Drawing.Size(181, 23);
            this.m_btnGenerateColumnList.TabIndex = 4;
            this.m_btnGenerateColumnList.Text = "Generate Column List";
            this.m_btnGenerateColumnList.UseVisualStyleBackColor = true;
            this.m_btnGenerateColumnList.Click += new System.EventHandler(this.m_btnGenerateColumnList_Click);
            // 
            // m_statusStrip
            // 
            this.m_statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.m_tsslError,
            this.m_tsProgressBar});
            this.m_statusStrip.Location = new System.Drawing.Point(0, 398);
            this.m_statusStrip.Name = "m_statusStrip";
            this.m_statusStrip.Size = new System.Drawing.Size(582, 22);
            this.m_statusStrip.TabIndex = 5;
            // 
            // m_tsslError
            // 
            this.m_tsslError.Name = "m_tsslError";
            this.m_tsslError.Size = new System.Drawing.Size(39, 17);
            this.m_tsslError.Text = "Ready";
            this.m_tsslError.Click += new System.EventHandler(this.m_tsslError_Click);
            // 
            // m_tsProgressBar
            // 
            this.m_tsProgressBar.Name = "m_tsProgressBar";
            this.m_tsProgressBar.Size = new System.Drawing.Size(100, 16);
            this.m_tsProgressBar.Visible = false;
            // 
            // m_lvDuplicates
            // 
            this.m_lvDuplicates.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.m_lvDuplicates.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.chFileName,
            this.m_chRowNumber,
            this.m_chGeneName});
            this.m_lvDuplicates.Location = new System.Drawing.Point(12, 298);
            this.m_lvDuplicates.Name = "m_lvDuplicates";
            this.m_lvDuplicates.Size = new System.Drawing.Size(558, 97);
            this.m_lvDuplicates.TabIndex = 6;
            this.m_lvDuplicates.UseCompatibleStateImageBehavior = false;
            this.m_lvDuplicates.View = System.Windows.Forms.View.Details;
            this.m_lvDuplicates.Visible = false;
            this.m_lvDuplicates.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.m_lvDuplicates_ColumnClick);
            // 
            // chFileName
            // 
            this.chFileName.Text = "FileName";
            this.chFileName.Width = 85;
            // 
            // m_chRowNumber
            // 
            this.m_chRowNumber.Text = "RowNumber";
            this.m_chRowNumber.Width = 93;
            // 
            // m_chGeneName
            // 
            this.m_chGeneName.Text = "GeneName";
            this.m_chGeneName.Width = 96;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(582, 420);
            this.Controls.Add(this.m_lvDuplicates);
            this.Controls.Add(this.m_statusStrip);
            this.Controls.Add(this.m_btnGenerateColumnList);
            this.Controls.Add(this.m_btnGenerateGeneList);
            this.Controls.Add(this.chartMain);
            this.Controls.Add(this.m_cmbBox);
            this.Controls.Add(this.mnuMain);
            this.MainMenuStrip = this.mnuMain;
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.chartMain)).EndInit();
            this.mnuMain.ResumeLayout(false);
            this.mnuMain.PerformLayout();
            this.m_statusStrip.ResumeLayout(false);
            this.m_statusStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        public System.Windows.Forms.ComboBox m_cmbBox;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartMain;
        private System.Windows.Forms.MenuStrip mnuMain;
        private System.Windows.Forms.ToolStripMenuItem tsmiInputFileType;
        private System.Windows.Forms.ToolStripMenuItem tsmiCSV;
        private System.Windows.Forms.ToolStripMenuItem tsmiXLSX;
        private System.Windows.Forms.Button m_btnGenerateGeneList;
        private System.Windows.Forms.Button m_btnGenerateColumnList;
        private System.Windows.Forms.StatusStrip m_statusStrip;
        private System.Windows.Forms.ToolStripProgressBar m_tsProgressBar;
        private System.Windows.Forms.ToolStripStatusLabel m_tsslError;
        private System.Windows.Forms.ListView m_lvDuplicates;
        private System.Windows.Forms.ColumnHeader chFileName;
        private System.Windows.Forms.ColumnHeader m_chRowNumber;
        private System.Windows.Forms.ColumnHeader m_chGeneName;
    }
}

